package ulco.cardGame.common.players;

import ulco.cardGame.common.players.BoardPlayer;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * creation de la classe    Cardplayer heritière de Boardpayer
 */

public class CardPlayer extends BoardPlayer {
    private  List<Card> cards;

    /**
     * constructeur de Cardplayer
     * @param name
     */
    public CardPlayer(String name){

        super(name);
    }

    /**
     * getteur de score
     * @return
     */

    public Integer getScore(){
        return this.score;
    }



    /**
     * fonction qui permet d'ajouter un  compoment
     * @param component
     */
    public void addComponent(Component component){
       // si la liste des cartes est nulle
        if (this.cards == null) {
            // on créé un arraylist temporaire
            List<Card> tempcards = new ArrayList<>();
            // on y ajoute le compement
            tempcards.add((Card) component);
            // puis on fait correspondre l'arraylist  a la liste des cartes
            this.cards= tempcards;
            // on implemente (on l'augmente) de 1
            this.score+=1;
        }
        //sinon si la liste des cartes n'est pas vide
        else{
            // on ajoute directement le compoment a la liste des cartes
            this.cards.add((Card) component);
            // on implemente (on l'augmente) de 1
            this.score+=1;
        }
    }

    /**
     * fonction qui permet de retirer un compoment
     * @param component
     */

    public void removeComponent(Component component){
        this.cards.remove( component);
        this.score-=1;
    }

    /**
     * supprime la première carte de la main
     * @return
     */

    public void  play(Socket socket) throws IOException {
        // recuperation de la  premiere carte en main
        Card card = this.cards.get(0);
        //le rend visible
        card.setHidden(true);
        //supprime la carte de ma main
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
        oos.writeObject(card);
        // this.cards.remove(card);
        //return card;
    }

    /**
     * getteurs de arraylist compoment
     * @return
     */
    public List<Component> getComponents(){

       return new ArrayList<>(this.cards);
    }

    /**
     * permet de melanger aléatoirement les cartes en main
     */
    @Override
    public void shuffleHand() {
        Collections.shuffle(this.cards);
    }

    /**
     * permet de supprimer les cartes en main
     */
    @Override
    public void clearHand() {
        this.cards.clear();
    }

    /**
     * affiche les informations du jeu
     * @return
     */

    public String toString(){

        return super.toString();
     }

    public void displayHand(){
        System.out.println("----------------");
        for(Card card : this.cards){
            String M = String.format("Name : %1s",card.getName());
            System.out.println(M);
        }
        System.out.println("----------------");
    }

     public List<Component> getSpecificComponents(Class classType){

             return new ArrayList<>(this.cards);


     }

}
