package ulco.cardGame.common.players;

import ulco.cardGame.common.interfaces.Player;

/**
 * creation de la classe Board player
 */
public abstract  class BoardPlayer implements Player {
   protected String name;
   protected boolean playing;
   protected  Integer score;

    /**
     * constructeur de la classe BoardPlayer
     * @param name
     */
   public  BoardPlayer(String  name){
       this.name = name;
       this.score=0;
       this.playing=false;

   }

    /**
     * methode pour notifier si le joueur peut jouer
     * @param playing
     */

   public void canPlay(boolean playing){

       this.playing=playing;
   }

    /**
     * getteurs de name retourne name
     * @return
     */

    public String getName(){

        return this.name;
    }

    /**
     * methode isplaying
     * @return
     */
    public boolean isPlaying(){
       return this.playing;
    }

    /**
     * getteur de score
     * @return
     */
    @Override
    public Integer getScore() {

        return this.score;
    }

    /**
     * methode string pour affichage
     * @return
     */


    public String toString(){

        return  String.format("Name =  '%1s', \n Score = %1d   \t",this.name,this.score );
    }
}
