/*
package ulco.cardGame.common.games;

import ulco.cardGame.common.players.BoardPlayer;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class PokerPlayer extends BoardPlayer {

    private List<Card>cards =  new ArrayList<>();
    private List<Coin>coins = new ArrayList<>();

    /**
     * constructeur de la classe BoardPlayer
     *
     * @param name
     */
/*
    public PokerPlayer(String name) {
        super(name);
    }

    /**
     * methode play
     * @return
     */
/*
    @Override
    public Component play()  {
        this.displayHand();

        Scanner scanner = new Scanner(System.in);
        Component coinToPlay = null;
        boolean correctCoin = false;

        do {

            System.out.println("[" + this.getName() + "], please select a valid Coin to play (coin color)");
            String value = scanner.nextLine();

            for (Coin coin : coins) {
                if (coin.getName().equals(value)) {
                    coinToPlay = coin;
                    correctCoin = true;
                }
            }

        } while(!correctCoin);



        // Remove card from  player hand

        this.removeComponent(coinToPlay);


    return coinToPlay;

    }

    /**
     * ajoute un component soit coin ou carte
     * @param component
     */
/*
    @Override
    public void addComponent(Component component) {
        if(component instanceof Card){
            this.cards.add((Card)component);
             this.score+=1;
        }
        else {
            this.coins.add((Coin)component);
          this.score+=component.getValue();

        }
    }

    /**
     * supprime un component soit coin ou carte
     * @param component
     */
/*
    @Override
    public void removeComponent(Component component) {
        if(component instanceof Card){
            this.cards.remove( (Card) component);

        }else{
            this.coins.remove( (Coin) component);
            this.score-=1;

        }


    }

    /**
     * getteur specifique qui retourne soit la liste des coins ou cartes
     * @param classType
     * @return
     */
/*
    public  List<Component> getSpecificComponents(Class classType){
        if(classType==Card.class){
            return new ArrayList<>(this.cards);
        }
        else {
            return new ArrayList<>(this.coins);
        }

    }

    /**
     * getteurs de la listes des componets coin et cartes
     * @return
     */
/*
    @Override
    public List<Component> getComponents() {
        List<Component> liste =  new ArrayList<>();
        liste.addAll(this.coins);
        liste.addAll(this.cards);
        return  liste;
    }

    /**
     * melange les cartes
     */
/*
    @Override
    public void shuffleHand() {
        Collections.shuffle(this.cards);

    }

    /**
     * supprime la main du joueur
     */
/*
    @Override
    public void clearHand() {
         this.cards.clear();
         this.coins.clear();
    }

    /**
     * affiche le contenu de la main du joueur
     */
/*
    public void displayHand(){
        int somme = 0;
        System.out.println("----------------");

        for(Card card : this.cards){
           System.out.println("Card : " + card.getName()) ;

        }
        System.out.println("----------------");

        for(Coin coin : this.coins){
          System.out.println(" - Coin  " + coin.getName() + "x" + coin.getValue());
          somme+=coin.getValue();
        }

        System.out.println("Your Coins sum is about :" + somme);
        System.out.println("----------------");
    }

    public String toString(){
        return this.toString();
    }
}
*/