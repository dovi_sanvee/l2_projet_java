package ulco.cardGame.common.interfaces;

import ulco.cardGame.common.players.BoardPlayer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;
import java.util.List;
import java.util.Map;

public interface Game extends Serializable {

    /**
     * Initialize the whole game using a parameter file
     * @param filename
     */
    void initialize(String filename) ;

    /**
     *
     * @return
     */
    Player run(Map<Player,Socket> map) throws IOException, ClassNotFoundException;

    /**
     * Add player to the current Game
     * @param player
     */
    boolean addPlayer( Player player,Socket socket) throws IOException;

    /**
     * Remove player from the game using the reference
     * @param player
     */
    void removePlayer(Player player);

    /**
     * Remove players from the game
     */
    void removePlayers();

    /**
     * Return player from current Game
     * @return
     */
    List<BoardPlayer> getPlayers();

    /**
     * Specify if game is started or not
     * @return
     */
    boolean isStarted();

    /**
     * current number of players inside the Game
     * @return
     */
    Integer maxNumberOfPlayers();

    /**or not
     */
    boolean end();

    /**
     * Display current Game state
     * Specify if the Game has end
     * Such as user state (name and score)
     */
    void displayState();

    /**
     * recupère le board
     * @return
     */

    Board getBoard();

    /**
     * recupere le joueur actuel
     * @param string
     * @return
     */
    Player getCurrentPlayer(String string);
}
