package ulco.cardGame.common.games.Boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;

import java.util.ArrayList;
import java.util.List;

public class PokerBoard implements Board {
  private List<Card>cards = new ArrayList<>();
  private List<Coin>coins = new ArrayList<>();

    /**
     * supprime les jetons et les cartes
     */
    @Override
    public void clear() {
           this.cards.clear();
           this.coins.clear();
    }

    /**
     * ajoute le component qu'il faut soit coin ou carte
     * @param component
     */
    @Override
    public void addComponent(Component component) {
        if(component instanceof Card){
            this.cards.add((Card)component);

        }
        else {
            this.coins.add((Coin)component);
        }
    }

    /**
     * getteur de tous les components
     * @return
     */

    @Override
    public List<Component> getComponent() {
         List<Component> liste =  new ArrayList<>();
         liste.addAll(this.coins);
         liste.addAll(this.cards);
         return  liste;
    }

    /**
     * getteur de component specifique soit coin ou carte
     * @param classType
     * @return
     */

    @Override
    public List<Component> getSpecificComponent(Class classType) {
       if(classType==Card.class){
         return new ArrayList<>(this.cards);
       }
       else {
           return new ArrayList<>(this.coins);
       }
    }

    /**
     * affichage des cartes et des coins
     */

    @Override
    public void displayState() {
        System.out.println("----------------");
        for(Card card : this.cards){
            String M = String.format("Name : %1s",card.getName());
            System.out.println(M);
        }
        System.out.println("----------------");
        for(Coin coin : this.coins){
            String M = String.format("Name : %1s  value : %1s",coin.getName(),coin.getValue());
            System.out.println(M);
        }
        System.out.println("----------------");
    }
}
