package ulco.cardGame.common.interfaces;




import ulco.cardGame.common.games.components.Component;

import java.io.Serializable;
import java.util.List;


public interface Board extends Serializable {
    /**
     * methode clear permettant de supprimer
     */
      void clear();

    /**
     * methode addComponent permettant d'ajouter un component
     * @param component
     */

     void addComponent(Component component);

    /**
     * methode getComponent permettant de recuperer une liste de component
     * @return
     */

      List<Component> getComponent();

    /**
     * methode getSpecificComponent permetant de recuperer un component spécifique
     * @param classType
     * @return
     */

      List<Component> getSpecificComponent(Class classType);

    /**
     * methode displaystate permettant d'afficher
     */

     void displayState();
}
