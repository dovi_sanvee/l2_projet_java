package ulco.cardGame.common.games;

import ulco.cardGame.common.games.Boards.CardBoard;
import ulco.cardGame.common.games.Boards.PokerBoard;
import ulco.cardGame.common.interfaces.Board;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.common.players.BoardPlayer;


import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public abstract class BoardGame implements Game {
    /**
     * les différents attributs de cette classe
     */
    protected  String name;
    protected Integer  maxPlayers;
    protected List<Player> players;
    protected boolean endGame;
    protected boolean started;
    protected Board board  = new CardBoard();
    /**
     * les methodes de cette classe
     */
    /**
     * constructeur de la classe BOARDGame
     * @param name
     * @param maxPlayers
     * @param filename
     */

    public BoardGame(String name, Integer maxPlayers, String filename) {

        this.name = name;
        this.maxPlayers = maxPlayers;
         initialize(filename);
         this.endGame=false;
         this.started = false;
    }

    /**
     * ajoute un joueur au jeu
     * @param player
     * @return
     */
    public boolean addPlayer(Player player, Socket socket) throws IOException {
        // on vérifie si la liste des joeurs est  null
        if (this.players == null){
            // on creer un arraylist temporaire
            List<Player> tempPlayer = new ArrayList<>();
            // on ajoute le joueur dans l'arraylist
            tempPlayer.add(player);
            // on lui notifie qu'il est apte a joué
            player.canPlay(true);
            // puis on fait correspondre l'arraylist a la liste des joueurs
            this.players = tempPlayer;
                ObjectOutputStream nn = new ObjectOutputStream(socket.getOutputStream());
                nn.writeObject("Player added into players game list ");
            if(this.getPlayers().size()==this.maxNumberOfPlayers()) this.started=true;
            return true;
        }
          // si le nombre de joueur dans la liste inférieur au nombre de joeur maximal
            if (this.players.size()<maxNumberOfPlayers()){
                // on parcours la liste des joeurs pour voir si le nom du joeur passé en paramètre correspond a un nom dans la liste
                for (Player pl : this.getPlayers()) {
                    // si ca correspond on retourne false on fait rien
                    if (pl.getName() == player.getName()) {
                        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                        oos.writeObject("Username player already taken");
                        return false;

                    }

                }
                // sinon si ca corespond a aucun nom dans la liste on l'ajoutye
                player.canPlay(true);
                this.players.add(player);
                ObjectOutputStream j = new ObjectOutputStream(socket.getOutputStream());
                j.writeObject("Player added into players game list ");
                if(this.getPlayers().size()==this.maxNumberOfPlayers()) this.started=true;
                return true;

            }
            // on mets la variable started a true si on atteint le nombre de joeur maximal
            ObjectOutputStream v = new ObjectOutputStream(socket.getOutputStream());
            v.writeObject(" Maximun number of player reached (max:" + maxPlayers+ ")");
            return false;
        }




    /**
     * supprime un joueur donné de la liste des joueurs
     * @param player
     */
    public void removePlayer(Player player){

        this.players.remove(player);
    }

    /**
     * supprime tous les joueurs de la liste des joueurs
     */
    public void removePlayers(){

        this.players.clear();
    }

    /**
     * methode d'affichage de l'état du jeu (nom et score de chaque joueur)
     */

    public void displayState(){
        System.out.println("--------------------------------");
        System.out.println("-----------GAME STATE-----------");
        System.out.println("--------------------------------");
        String Affichage = " ";
              for (Player pl : this.players){
                 Affichage  =  String.format("%1s{name = %s, \t ScoreJoueur = %s}\t",pl.getClass().getSimpleName(),pl.getName(),pl.getScore());
                System.out.println(Affichage);
              }
        System.out.println("--------------------------------");
    }

    /**
     * methode pour savoir si le jeu commence ou nom
     * @return
     */
    public boolean isStarted(){

        return this.started;
    }

    /**
     * retourne le nombre de joeurs
     * @return
     */

    public Integer maxNumberOfPlayers(){

        return this.maxPlayers;
    }

    /**
     * retourne la  liste  des joueurs
     * @return
     */
    public List<BoardPlayer> getPlayers(){
      List<BoardPlayer>bp = new ArrayList<>();
      for (Player p : this.players){
          bp.add((BoardPlayer) p);
      }

        return bp;
    }

    @Override
    public Board getBoard() {
        return this.board;
    }

    @Override
    public Player getCurrentPlayer(String string) {
        for(Player pl : this.getPlayers()){
            if(pl.getName().equals(string)){
                return pl;
            }
        }
        return null;
    }

}
