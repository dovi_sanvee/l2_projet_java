package ulco.cardGame.common.games.components;

public  class Card  extends Component{
    /**
     * definition d'un attribut booleen hidden
     */
    private boolean hidden;

    /**
     * contrusteur de la composante Card
     * @param name
     * @param value
     * @param hidden
     */
    public Card(String name, Integer value, boolean hidden) {
        super(name, value);
        this.hidden=hidden;
    }

    /**
     * getteur de l'attribut hidden
     * @return
     */

    public boolean isHidden() {

        return this.hidden;
    }

    /**
     * setteurs de l'attribut hidden
     * @param hidden
     */

    public void setHidden(boolean hidden) {

        this.hidden = hidden;
    }

    /**
     * affiche les informations de la carte
     * @return
     */
    public String toString(){
        return  "nomdeCarte : \t" + this.name + "Value : \t" + this.value  ;
    }
}
