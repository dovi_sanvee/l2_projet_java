package ulco.cardGame.common.games.Boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;

import javax.swing.border.Border;
import java.util.ArrayList;
import java.util.List;

public class CardBoard implements Board {

    private List<Card> cards = new ArrayList<>();

    /**
     * supprime toutes  les cartes
     */
    @Override
    public void clear() {
        this.cards.clear();

    }

    /**
     * ajoute un componemt
     * @param component
     */
    @Override
    public void addComponent(Component component) {
        this.cards.add((Card)component);
    }

    /**
     * getteur de component
     * @return
     */
    @Override
    public List<Component> getComponent() {
        return new ArrayList<>(this.cards);
    }

    /**
     * getteur de liste de component specifique
     * @param classType
     * @return
     */

    @Override
    public List<Component> getSpecificComponent(Class classType) {
        return new ArrayList<>(this.cards);
    }

    /**
     * affichage du component
     */
    @Override
    public void displayState() {
        System.out.println("----------------");
        for(Card card : this.cards){
            String M = String.format("Name : %1s",card.getName());
            System.out.println(M);
        }
        System.out.println("----------------");
    }

}
