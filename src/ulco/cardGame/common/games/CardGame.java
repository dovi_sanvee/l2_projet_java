package ulco.cardGame.common.games;

import ulco.cardGame.common.games.Boards.CardBoard;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;


import java.io.*;

import java.net.Socket;
import java.util.*;

public  class CardGame  extends BoardGame {

    private List<Card> cards;
    private int numberOfRounds =0;



    /**
     * constructeur de la classe BOARDGame
     *
     * @param name
     * @param maxPlayers
     * @param filename
     */
    public CardGame(String name, Integer maxPlayers, String filename)  {
        super(name,maxPlayers,filename);

    }

    /**
     * charge le fichier de jeu et ajouet les cartes a une liste
     * @param filename
     */
    @Override
    public void initialize(String filename) {

        this.cards = new ArrayList<>();
        this.numberOfRounds = 0;

        // Here initialize the list of Cards
        try {
            File cardFile = new File(filename);
            Scanner myReader = new Scanner(cardFile);

            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();
                String[] dataValues = data.split(";");

                // get Card value
                Integer value = Integer.valueOf(dataValues[1]);
                this.cards.add(new Card(dataValues[0], value, true));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    @Override

    public Player run(Map<Player, Socket> map) throws IOException, ClassNotFoundException {
        //Melange des  cartes aleatoirement
        Collections.shuffle(this.cards);
        //Distribuons les cartes à  chacun  des joueurs du jeu
        int i = 0;
        //distribution des cartes
        for (Card card : this.cards) {
            this.getPlayers().get(i).addComponent(card);
            i += 1;
            //recommence le partage des cartes a chaque joueurs
            if (i == this.getPlayers().size()) {
                i = 0;
            }
        }
        for (Socket so : map.values()) {
            ObjectOutputStream oos = new ObjectOutputStream(so.getOutputStream());
            oos.writeObject(this);
        }

        //les joueurs s'affrontent entre eux
        List<Player> vaiq = new ArrayList<>();
        while (!this.end()) {
            // creation d'un hashmap pour les joureurs et les cartes
            Map<Player, Card> playedCard = new HashMap<>();
            // boucle pour parcourir les joueurs
            for (Map.Entry<Player, Socket> mp : map.entrySet()) {
                if (mp.getKey().isPlaying()) {
                    for (Socket sc : map.values()) {
                        if (sc.equals(mp.getValue())) continue;
                        ObjectOutputStream oos1 = new ObjectOutputStream(sc.getOutputStream());
                        oos1.writeObject(" waiting for " + mp.getKey().getName() + " to play... ");

                    }
                      Map<String,CardGame>p= new Hashtable<>();
                    p.put("{"+ mp.getKey().getName()+ "}" + " you have to play" ,this);
                    ObjectOutputStream k = new ObjectOutputStream(mp.getValue().getOutputStream());
                    k.writeObject(p);
                    ObjectInputStream oos4 = new ObjectInputStream(mp.getValue().getInputStream());
                    Card c = (Card)oos4.readObject();
                    playedCard.put(mp.getKey(), c);
                  this.getBoard().addComponent((Card)c);
                    mp.getKey().removeComponent(c);
                    for (Socket cs : map.values()) {
                        if (cs.equals(mp.getValue())) continue;
                        ObjectOutputStream oo = new ObjectOutputStream(cs.getOutputStream());
                        oo.writeObject("Player  " + mp.getKey().getName() + "  has played  "+ c.getName());
                    }

                }
            }
            String msg = "----------Board State------------\n";
            for (Map.Entry<Player, Card> c : playedCard.entrySet()) {
                msg += String.format("%s played by %s\n", c.getValue().getName(), c.getKey().getName());
            }
            msg += "-----------------------------\n";

            for (Socket s : map.values()) {
                ObjectOutputStream so = new ObjectOutputStream(s.getOutputStream());
                so.writeObject(msg);
            }
            //verifier quel joueur doit remporter le round
            // initialisation du nombre de cartes maximal a 0
            int CardMaximal = 0;
            Card cardh = null;
            for (Map.Entry<Player, Card> entry : playedCard.entrySet()) {
                Player play = entry.getKey();
                //recupere la valeur de la carte du joueur
                Card c = entry.getValue();
                //si la valeur de la carte du joeur superieur a cardmaximal
                if (c.getValue() >= CardMaximal) {
                    if (CardMaximal == c.getValue()) {

                        vaiq.add(play);
                    } else {
                        // ajoute a cardMaximal le nombre de card
                        CardMaximal = c.getValue();
                        cardh = c;
                        if (!vaiq.isEmpty()) vaiq.clear();
                        vaiq.add(play);
                    }
                }
            }
            //ramassons  les cartes  par le vainqueur de round
            int p;
           if(vaiq.size() == 1) {
                p =0;
           }
            else {
                p=new Random().nextInt(vaiq.size());
            }

            for (Component c : this.getBoard().getSpecificComponent(Card.class)) {
                vaiq.get(p).addComponent(c);
            }
            String msgWin = String.format("Player :%s won the round with %s", vaiq.get(p).toString(), cardh.toString());
            for (Socket ss : map.values()) {
                ObjectOutputStream op = new ObjectOutputStream(ss.getOutputStream());
                op.writeObject(msgWin);
            }
            this.getBoard().clear();
            //melangeons   les cartes des joueurs apres 10 rounds
            if (this.numberOfRounds % 10 == 0) {
                // parcourir les joueur et melange la carte
                for (Player pl : this.getPlayers()) {
                    if (pl.getScore() == 0)
                        pl.canPlay(false);
                    if (pl.isPlaying())
                        pl.shuffleHand();
                }
            }
        }

        //retournous  le gagnant du jeu et affichons
        for (Player p : this.getPlayers()) {
            if (p.getScore() == this.cards.size())
                return p;
        }
        return null;
    }





    /**
     * implémente la fonction run selon les étapes ou regle de jeu  listé au 3.1
     * @return
     */
       /**!
        public Player run () {
            //Melange des  cartes aleatoirement
            Collections.shuffle(this.cards);
            //Distribuons les cartes à  chacun  des joueurs du jeu
            int i =0;
            //distribution des cartes
            for(Card card :this.cards){
                this.getPlayers().get(i).addComponent(card);
                i+=1;
                //recommence le partage des cartes a chaque joueurs
                if(i==this.getPlayers().size()){
                    i=0;
                }
            }
           //les joueurs s'affrontent entre eux
            List<Player> vaiq =new ArrayList<>();
            while(!this.end()){
                // tant que le jeu n'est pas fini on continue la partie
                System.out.println("ROUND "+this.numberOfRounds+1);
                //affichage  des stats de chaque joueur
                this.displayState();
                // creation d'un hashmap pour les joureurs et les cartes
                Map<Player,Card>playedCard=new HashMap<>();
                // boucle pour parcourir les joueurs
                for(Player pl :this.getPlayers()){
                    if(pl.isPlaying())
                    {
                        Card card=(Card)pl.play();
                        playedCard.put(pl,card);
                        System.out.printf("%1s has played %1s \n",pl.getName(),card.getName());// affiche le joueur et la carte a jouer
                    }
                   // else
                 //   {
                        // supprimer le joueur
                    //    this.removePlayer(pl);
                   // }
                }
                //verifier quel joueur doit remporter le round
                // initialisation du nombre de cartes maximal a 0
                int CardMaximal = 0;

                // for each pour parcourir les cartes jouées
                for(Map.Entry<Player,Card>entry:playedCard.entrySet()){
                    Player play = entry.getKey();
                    //recupere la valeur de la carte du joueur
                    Card c =entry.getValue();
                    //si la valeur de la carte du joeur superieur a cardmaximal
                    if(c.getValue()>=CardMaximal){
                        if(CardMaximal==c.getValue()){

                            vaiq.add(play);
                        }
                        else
                        {
                            // ajoute a cardMaximal le nombre de card
                            CardMaximal=c.getValue();
                            if(!vaiq.isEmpty())vaiq.clear();
                            vaiq.add(play);
                        }
                    }
                }
                //ramassons  les cartes  par le vainqueur de round
                if(vaiq.size()==1) {
                    // boucle pour parcourir les cartes
                    for(Map.Entry<Player,Card>entry : playedCard.entrySet()){
                        vaiq.get(0).addComponent(entry.getValue());
                        // changeons le statut du player
                        if(entry.getKey().getComponents().isEmpty()) entry.getKey().canPlay(false);

                    }
                } else {
                    // affiche l'égalité et tire un gagant aleatoirement
                    System.out.println("il y a une égalité entre joueurs");
                    this.displayState();
                    //generer un gangant car ya plusieurs gagnant
                    int longueur = vaiq.size();
                    int Gagnantaleatoire = new Random().nextInt(longueur);
                    //ramassons les cartes  gagner par le vainqueur du round actuelle ou du tour actuelle
                    for(Map.Entry<Player,Card>entry:playedCard.entrySet()){
                        vaiq.get(Gagnantaleatoire).addComponent(entry.getValue());
                        //modifions  le statut du joueur s'il lorsqu'il  n'y a plus de carte
                        if(entry.getKey().getComponents().size()==0) entry.getKey().canPlay(false);

                    }
                }
                //melangeons   les cartes des joueurs apres 10 rounds
                if(this.numberOfRounds%10==0){
                    // parcourir les joueur et melange la carte
                    for(Player pl:this.getPlayers()){
                        pl.shuffleHand();
                    }
                }
                this.numberOfRounds+=1;
            }
            //retournous  le gagnant du jeu et affichons
            this.endGame=true;
            return vaiq.get(0);
        }
        */




    /**
     * verifie si le jeu est fini ou non
     * @return
     */

    @Override
        public boolean end() {
         for(Player pl : this.players){
             if(pl.getScore()== this.cards.size()){
                 return true;
             }
         }
         return false;
        }

    /**
     * affiche les informations du jeu
     * @return
     */
    public String toString(){

         return  this.getClass().getSimpleName() + "{name :  " + this.name +"}" ;
    }

}
