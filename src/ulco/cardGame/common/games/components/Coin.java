package ulco.cardGame.common.games.components;

public class Coin extends Component{
    /**
     * constructeur de Coin
     * @param name
     * @param value

     */
    public Coin(String name, Integer value ) {
        super(name, value);

    }
    public String toString(){
        return  "Coin  : \t" + name + "Value : \t" + value  ;
    }
}
